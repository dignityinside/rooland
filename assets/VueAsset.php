<?php

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Vue asset
 *
 * @package app\assets
 *
 * @author Alexander Schilling
 */
class VueAsset extends AssetBundle
{
    public $publishOptions = ['forceCopy' => true];
    public $sourcePath = 'js';
    public $baseUrl = '@web';

    public function init()
    {
        parent::init();

        $this->js[] = 'app.js';
    }
}

<?php

use app\components\UserPermissions;
use app\helpers\Text;
use yii\helpers\Html;
use yii\helpers\Url;
use \yii\widgets\ListView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\post\PostSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = \Yii::$app->params['site']['name'];

$this->registerMetaTag(['name' => 'title', 'content' => \Yii::$app->params['site']['name']]);
$this->registerMetaTag(['name' => 'description', 'content' => \Yii::$app->params['site']['description']]);
$this->registerMetaTag(['name' => 'robots', 'content' => 'index, follow, max-snippet:-1, max-image-preview:large, max-video-preview:-1']);

$this->registerLinkTag(['rel' => 'canonical', 'href' => Url::to(\Yii::$app->params['site']['url'])]);
?>
<div class="post-index">

    <div class="post-header text-center">
        <h1>
            <?php if (UserPermissions::canAdminPost()) : ?>
                <?= Html::a('<i class="fas fa-feather" style="color: #fff"></i>', ['/post/create']) ?>
            <?php else : ?>
                <i class="fas fa-feather"></i>
            <?php endif; ?>
            <?= \Yii::$app->params['site']['name'] ?>
        </h1>
    </div>

    <material-sort
        :data='`<?= Text::arrayToJson([
            [
                'icon' => 'fa fa-clock',
                'name' => \Yii::t('app', 'sort_new'),
                'url'  => Url::toRoute(['/post/index'])
            ],
            [
                'icon' => 'fa fa-eye',
                'name' => \Yii::t('app', 'sort_hits'),
                'url'  => Url::toRoute(['/post/index/1'])
            ],
            [
                'icon' => 'fa fa-comments',
                'name' => \Yii::t('app', 'sort_comments'),
                'url'  => Url::toRoute(['/post/index/2'])
            ],
        ]); ?>`'
    ></material-sort>

    <?php Pjax::begin(); ?>
      <?= ListView::widget(
          [
              'dataProvider' => $dataProvider,
              'emptyText'    => \Yii::t('app/blog', 'records_not_found'),
              'itemView'     => '_view',
              'layout'       => "{items}{pager}",
          ]
      ); ?>
    <?php Pjax::end(); ?>
</div>

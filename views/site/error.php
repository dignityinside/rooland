<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */

/* @var $exception Exception */

use app\components\UserPermissions;
use yii\helpers\Html;

$this->title = '404. Страница не найдена';
$this->params['breadcrumbs'][] = $this->title;

$this->registerMetaTag(['name' => 'robots', 'content' => 'noindex']);

?>
<div class="site-error">
    <h1><?= Yii::$app->response->statusCode; ?>. <?= $exception->getMessage(); ?></h1>

    <p><?= Html::a('Перейти на главную', ['post/index'], ['class' => 'btn btn-success']) ?></p>

    <?php if (UserPermissions::canAdminPost()) : ?>
        <p><?= Html::a('Создать страницу', ['/post/create'], ['class' => 'btn btn-success']) ?></p>
    <?php endif; ?>
</div>

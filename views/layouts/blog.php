<?php

use app\models\ad\Ad;
use app\models\category\Category;
use app\models\Material;

$this->beginContent('@app/views/layouts/main.php');

?>

<div class="row">
    <div class="col-md-3">
        <widget-categories
                :title='`<?= \Yii::t('app', 'sidebar_category'); ?>`'
                :list='`<?= json_encode(Category::getCategoriesList(Material::MATERIAL_POST_ID)) ?>`'
        ></widget-categories>

        <widget-social
                :title='`<?= \Yii::t('app/blog', 'social_networking'); ?>`'
                :list='`<?= json_encode(\Yii::$app->params['social'] ?? '') ?>`'>
        </widget-social>
    </div>
    <div class="col-md-6">
        <?= $content; ?>
    </div>
    <div class="col-md-3">
        <?= $this->render('./partials/sidebar/interesting.php'); ?>

        <widget-ad
            :title='`Реклама`'
            :position='`<?= Ad::SLOT_SIDEBAR; ?>`'
            :list='`<?= json_encode(Ad::getRandomAd(Ad::SLOT_SIDEBAR)) ?>`'
        >
        </widget-ad>
    </div>
</div>

<?php $this->endContent(); ?>

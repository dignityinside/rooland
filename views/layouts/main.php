<?php

/* @var $this yii\web\View */
/* @var $content string */

use app\assets\VueAsset;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
VueAsset::register($this);

$googleSiteVerification = \Yii::$app->params['googleSiteVerification'];
$yandexVerification = \Yii::$app->params['yandexVerification'];

?>
<?php $this->beginPage() ?>
  <!DOCTYPE html>
  <html lang="<?= Yii::$app->language ?>" prefix="og: http://ogp.me/ns#">
  <head>
      <meta charset="<?= Yii::$app->charset ?>">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <?php if ($googleSiteVerification) : ?>
          <meta name="google-site-verification" content="<?=\Yii::$app->params['googleSiteVerification'];?>">
      <?php endif; ?>
      <?php if ($yandexVerification) : ?>
          <meta name="yandex-verification" content="<?=\Yii::$app->params['yandexVerification'];?>" />
      <?php endif; ?>
      <?= Html::csrfMetaTags() ?>
      <title><?= Html::encode($this->title) ?></title>
      <?php $this->head() ?>
      <link rel="apple-touch-icon" sizes="60x60" href="/apple-touch-icon.png">
      <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
      <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
      <link rel="manifest" href="/site.webmanifest">
      <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#8fca58">
      <meta name="msapplication-TileColor" content="#8fca58">
      <meta name="theme-color" content="#ffffff">
      <?= $this->render('partials/head.php'); ?>
  </head>
  <body>
    <?php $this->beginBody() ?>

        <div class="wrap" id="app">
            <?= $this->render('partials/nav.php'); ?>

            <div class="container-fluid">
                <?= Breadcrumbs::widget(['links' => $this->params['breadcrumbs'] ?? []]) ?>
                <div class="alert alert-danger" style="text-align: center">
                    Руланд блог переезжает в Telegram <a href="https://t.me/rooland">@rooland</a>, подпишись если хочешь читать новые статьи там!
                    <a href="/ruland-blog-pereezzhaet-v-telegram">Подробнее →</a>
                </div>
                <?= $content ?>
            </div>

            <footer-block
                :date='`<?= date('Y'); ?>`'
                :name='`<?= \Yii::$app->params['site']['name']; ?>`'
                :links='`<?= json_encode([
                    [
                        'name' => \Yii::t('app', 'footer_about_link'),
                        'url' => '/about'
                    ],
                    [
                        'name' => \Yii::t('app', 'footer_premium_link'),
                        'url' => '/premium'
                    ],
                    [
                        'name' => \Yii::t('app', 'footer_contact_link'),
                        'url' => '/contact'
                    ],
                ]) ?>`'
            ></footer-block>
        </div>

    <?php
        $this->endBody();

        if (YII_ENV == YII_ENV_PROD) {
            echo $this->render('partials/counter.php');
        }
    ?>

    </body>
  </html>
<?php $this->endPage() ?>

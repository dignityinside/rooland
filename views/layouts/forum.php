<?php

use app\helpers\Text;
use app\models\ad\Ad;
use app\models\Comment;
use app\models\forum\Forum;
use yii\helpers\Markdown;

$this->beginContent('@app/views/layouts/main.php');

$newComments = Comment::getNewComments();

foreach ($newComments as $key => $value) {
    $newComments[$key]['text'] = strip_tags(Markdown::process($value['text'], 'gfm'));
}

?>

<div class="row">
    <div class="col-md-3">
        <widget-forum-topics
                :title='`Новые темы`'
                :list='`<?= Text::arrayToJson(Forum::getNewTopics()); ?>`'
        >
        </widget-forum-topics>

        <widget-forum-comments
                :title='`Новые ответы`'
                :list='`<?= Text::arrayToJson($newComments) ?>`'
        >
        </widget-forum-comments>
    </div>
    <div class="col-md-6">
        <?= $content; ?>
    </div>
    <div class="col-md-3">
        <widget-forum-topics
            :title='`Популярные темы`'
            :list='`<?= Text::arrayToJson(Forum::getPopularTopics()) ?>`'
            :filter='`hits`'
        >
        </widget-forum-topics>

        <widget-ad
            :title='`Реклама`'
            :position='`<?= Ad::SLOT_SIDEBAR; ?>`'
            :list='`<?= Text::arrayToJson(Ad::getRandomAd(Ad::SLOT_SIDEBAR)) ?>`'
        >
        </widget-ad>
    </div>
</div>

<?php $this->endContent(); ?>

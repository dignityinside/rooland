<?php

use app\widgets\Links;

?>

<widget>
    <div slot="title">
        <?= \Yii::t('app', 'sidebar_interesting'); ?>
    </div>
    <div slot="content">
        <?= Links::widget([
            'items' => [
                [
                    'label' => 'Lifehacker',
                    'url' => 'https://lifehacker.ru',
                    'rel' => 'nofollow noopener',
                    'target' => '_blank'
                ],
                [
                    'label' => 'Pepper',
                    'url' => 'https://www.pepper.ru',
                    'rel' => 'nofollow noopener',
                    'target' => '_blank'
                ],
                [
                    'label' => 'Steambuy',
                    'url' => 'https://steambuy.com/partner/182122',
                    'rel' => 'nofollow',
                    'target' => '_blank'
                ],
                [
                    'label' => 'g2a',
                    'url' => 'https://www.g2a.com/r/gr-5dd7b53b1faa0',
                    'rel' => 'nofollow',
                    'target' => '_blank'
                ],
            ],
        ]); ?>
    </div>
</widget>

<?php

    use app\components\UserPermissions;
    use yii\bootstrap\Nav;
    use yii\bootstrap\NavBar;
    use yii\helpers\Html;

    NavBar::begin(
        [
            'brandLabel' => Html::img('/img/rooland-logo.png'),
            'options'    => [
                'class' => 'navbar-inverse',
            ],
        ]
    );

    $menuItems[] = ['label' => \Yii::t('app/blog', 'menu_label_index_blog'), 'url' => ['/post/index']];

    // $menuItems[] = ['label' => \Yii::t('app/forum', 'menu_label_forum_index'), 'url' => ['/forum/index']];

    if (Yii::$app->user->isGuest) {
        $menuItems[] = ['label' => \Yii::t('app', 'menu_label_login'), 'url' => ['/login']];
    } else {

        $menuItems[] = [
            'label' => \Yii::t('app', 'menu_label_profile'),
            'url' => ['/user/view', 'id' => \Yii::$app->user->id]
        ];

        if (Yii::$app->user->can('admin')) {

            $menuItems[] = [
                'label' => \Yii::t('app', 'menu_label_panel'), 'items' => [
                    [
                        'label' => \Yii::t('app/blog', 'menu_label_admin_blog'),
                        'url' => ['/post/admin'],
                        'visible' => UserPermissions::canAdminPost()
                    ],
                    [
                        'label' => \Yii::t('app/comments', 'menu_label_admin_comments'),
                        'url' => ['/comment-admin/manage/index'],
                        'visible' => UserPermissions::canAdminPost()
                    ],
                    [
                        'label' => \Yii::t('app/forum', 'menu_label_forum_admin'),
                        'url' => ['/forum/admin'],
                        'visible' => UserPermissions::canAdminForum()
                    ],
                    [
                        'label' => \Yii::t('app/category', 'menu_label_admin_category'),
                        'url' => ['/category/admin'],
                        'visible' => UserPermissions::canAdminCategory()
                    ],
                    [
                        'label' => \Yii::t('app', 'menu_label_user_admin'),
                        'url' => ['/user/admin'],
                        'visible' => UserPermissions::canAdminUsers()
                    ],
                    [
                        'label' => \Yii::t('app', 'menu_label_ad_admin'),
                        'url' => ['/ad/admin'],
                        'visible' => UserPermissions::canAdminAd()
                    ],
                ],
            ];

        }

        $menuItems[] = [
            'label' => \Yii::t('app', 'logout_({username})', [
                'username' => Yii::$app->user->identity->username,
            ]),
            'url' => ['/site/logout'],
            'linkOptions' => ['data-method' => 'post']
        ];

    }

    echo Nav::widget(
        [
            'options' => ['class' => 'navbar-nav navbar-left'],
            'items'   => $menuItems,
        ]
    );

    echo $this->render('../partials/search.php');

    NavBar::end();

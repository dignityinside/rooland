<?php

use app\components\UserPermissions;
use app\helpers\Text;
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use yii\helpers\Markdown;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\forum\Forum */

$this->title = Html::encode($model->title);

$this->params['breadcrumbs'][] = ['label' => \Yii::t('app/forum', 'breadcrumbs_forum_index'),
                                  'url' => ['/forum/index']];
$this->params['breadcrumbs'][] = ['label' => $model->category->name,
                                  'url' => ['/forum/topics', 'categoryName' => $model->category->slug]];
$this->params['breadcrumbs'][] = $this->title;

$this->registerMetaTag(['name' => 'title', 'content' => $model->title]);
$this->registerMetaTag(['name' => 'description', 'content' => Html::encode($model->meta_description)]);
$this->registerMetaTag(['name' => 'robots', 'content' => 'index, follow, max-snippet:-1, max-image-preview:large, max-video-preview:-1']);

$this->registerLinkTag(['rel' => 'canonical', 'href' => Url::to(['topic', 'id' => $model->id], true)]);

if (!empty($model->user_id)) {
    $user['link'] = Url::toRoute(['forum/user/' . $model->user->username]);
    $user['name'] = $model->user->username;
} else {
    $user['link'] = '';
    $user['name'] = \Yii::t('app', 'user_anonym');
}

?>

<forum-topics-item
    :title='`<?= $this->title; ?>`'
    :data='`<?= Text::arrayToJson([
        'edit'       => [
            'canEdit' => UserPermissions::canAdminForum() || UserPermissions::canEditForum($model),
            'link'    => Url::toRoute(['forum/update', 'id' => $model->id]),
            'text'    => \Yii::t('app', 'button_update'),
        ],
        'id'         => $model->id,
        'createdAt'  => $model->getFormattedCreatedAt(),
        'shareTitle' => \Yii::t('app', 'share_friends'),
        'user'       => $user,
        'category'   => [
            'name' => $model->category->name,
            'link' => Url::toRoute(['/forum/topics/' . $model->category->slug]),
        ],
    ]); ?>`'
>
    <div slot="content">
        <?= HtmlPurifier::process(Markdown::process($model->content, 'gfm'), ['HTML.Nofollow' => true]) ?>
    </div>
    <div slot="comments">
        <?= $this->render('_comments', ['model' => $model]); ?>
    </div>
</forum-topics-item>
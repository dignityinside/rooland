<?php

use yii\helpers\Html;
use yii\widgets\ListView;

/** @var $this yii\web\View */
/** @var $dataProvider yii\data\ActiveDataProvider */
/** @var string $userName */

$this->title = \Yii::t('app/forum', 'forum_user_topics') . ' ' . Html::encode($userName);

$this->params['breadcrumbs'][] = ['label' => \Yii::t('app/forum', 'breadcrumbs_forum_index'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>

<forum-topics-list
    :title='`<?= $this->title; ?>`'>
    <div slot="list-view">
        <?= ListView::widget(
            [
                'dataProvider' => $dataProvider,
                'emptyText' => \Yii::t('app/forum', 'forum_topics_list_empty_text'),
                'itemView' => '_index_topics',
                'layout' => "{items}{pager}",
            ]
        ); ?>
    </div>
</forum-topics-list>

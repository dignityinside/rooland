<?php

use app\helpers\Text;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var \app\models\category\Category $categoryModel */

$categoryName = isset($categoryModel)
    ? Html::encode($categoryModel->name)
    : \Yii::t('app/forum', 'forum_new_topics_text');
$categoryId = isset($categoryModel) ? $categoryModel->id : 0;
$categorySlug = isset($categoryModel) ? $categoryModel->slug : 'new';

$this->title = isset($categoryModel)
    ? \Yii::t('app/forum', 'page_forum_topic_title') . ' - ' . $categoryName
    : \Yii::t('app/forum', 'forum_new_topics_text');

if (isset($categoryModel)) {
    $this->registerMetaTag(['name' => 'description', 'content' => $categoryModel->description]);
    $this->registerMetaTag(['name' => 'robots', 'content' => 'index, follow, max-snippet:-1, max-image-preview:large, max-video-preview:-1']);

    $this->registerLinkTag(['rel' => 'canonical', 'href' => Url::to(['/forum/topics/' . $categoryModel->slug], true)]);
}

$this->params['breadcrumbs'][] = ['label' => \Yii::t('app/forum', 'breadcrumbs_forum_index'), 'url' => ['index']];
$this->params['breadcrumbs'][] = isset($categoryModel) ? $categoryName : \Yii::t('app/forum', 'forum_new_topics_text');

?>

<forum-topics-list
    :title='`<?= $this->title; ?>`'
    :buttons='`<?= Text::arrayToJson([
        [
            'icon' => 'fas fa-plus',
            'name' => \Yii::t('app/forum', 'forum_button_new_topic'),
            'url' => Url::toRoute(['create', 'id' => $categoryId]),
            'class' => 'btn btn-success',
        ],
        [
            'icon' => 'fa fa-folder',
            'name' => \Yii::t('app/forum', 'forum_button_categories'),
            'url' => Url::toRoute(['/forum']),
            'class' => 'btn btn-default',
        ],
        [
            'icon' => 'fa fa-clock',
            'name' => \Yii::t('app/forum', 'forum_button_new_topics'),
            'url' => Url::toRoute(['topics', 'categoryName' => 'new']),
            'class' => 'btn btn-default',
        ],
        [
            'icon' => 'fas fa-crown',
            'name' => \Yii::t('app/forum', 'forum_button_my_topics'),
            'url' => Url::toRoute(['my']),
            'class' => 'btn btn-default',
        ],
    ]); ?>`'
    :material-sort='`<?= Text::arrayToJson([
        [
            'icon' => 'fa fa-clock',
            'name' => \Yii::t('app', 'sort_new'),
            'url' => Url::toRoute(['/forum/topics/', 'categoryName' => $categorySlug])
        ],
        [
            'icon' => 'fa fa-eye',
            'name' => \Yii::t('app', 'sort_hits'),
            'url' => Url::toRoute(['/forum/topics/' . $categorySlug . '/hits'])
        ],
        [
            'icon' => 'fa fa-comments',
            'name' => \Yii::t('app', 'sort_comments'),
            'url' => Url::toRoute(['/forum/topics/' . $categorySlug . '/comments'])
        ],
        [
            'icon' => 'fas fa-comment-slash',
            'name' => \Yii::t('app/forum', 'forum_sort_no_answer'),
            'url' => Url::toRoute(['/forum/topics/' .  $categorySlug . '/unanswered'])
        ],
    ]); ?>`'
    :material-show="`<?= $dataProvider->totalCount >= 2; ?>`"
>
    <div slot="list-view">
        <?= ListView::widget(
            [
                'dataProvider' => $dataProvider,
                'emptyText'    => \Yii::t('app/forum', 'forum_category_list_empty_text'),
                'itemView'     => '_index_topics',
                'layout'       => "{items}{pager}",
            ]
        ); ?>
    </div>
</forum-topics-list>

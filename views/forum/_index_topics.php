<?php

use app\helpers\Text;
use yii\helpers\Url;

/** @var $this yii\web\View */
/** @var $model app\models\forum\Forum */

if (!empty($model->user_id)) {
    $user['link'] = Url::toRoute(['forum/user/' . $model->user->username]);
    $user['name'] = $model->user->username;
} else {
    $user['link'] = '';
    $user['name'] = \Yii::t('app', 'user_anonym');
}

?>

<forum-topics-items
    :data='`<?= Text::arrayToJson([
        'pinned'         => $model->pinned ? true : false,
        'allow_comments' => $model->allow_comments ? true : false,
        'title'          => $model->title,
        'url'            => Url::toRoute(['forum/topic', 'id' => $model->id]),
        'createdAt'      => $model->getFormattedCreatedAt(),
        'user'       => $user,
        'category'   => [
            'name' => $model->category->name,
            'link' => Url::toRoute(['/forum/topics/' . $model->category->slug]),
        ],
        'comments' => $model->commentsCount,
        'hits' => $model->hits
    ]); ?>`'
></forum-topics-items>

<?php

use app\helpers\Text;
use app\models\forum\Forum;
use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = \Yii::t('app/forum', 'page_forum_index_title');

$this->registerMetaTag(['name' => 'description', 'content' => 'Форум сайта ' . \Yii::$app->params['site']['name']]);
$this->registerMetaTag(['name' => 'robots', 'content' => 'index, follow, max-snippet:-1, max-image-preview:large, max-video-preview:-1']);

$this->registerLinkTag(['rel' => 'canonical', 'href' => Url::to(['/forum'], true)]);

$this->params['breadcrumbs'][] = \Yii::t('app/forum', 'breadcrumbs_forum_index');

?>

<forum-categories-list-item
    :title='`<?= $this->title; ?>`'
    :empty-text='`<?= \Yii::t('app/forum', 'forum_index_list_empty_text'); ?>`'
    :list='`<?= Text::arrayToJson(Forum::getCategoriesList()); ?>`'
    :buttons='`<?= Text::arrayToJson([
        [
            'name'  => \Yii::t('app/forum', 'forum_button_new_topic'),
            'url'  => Url::toRoute(['create']),
            'icon'  => 'fas fa-plus',
            'class' => 'btn btn-success',
        ],
        [
            'name'  => \Yii::t('app/forum', 'forum_button_new_topics'),
            'url'  => Url::toRoute(['topics', 'categoryName' => 'new']),
            'icon'  => 'fa fa-clock',
            'class' => 'btn btn-default',
        ],
        [
            'name'  => \Yii::t('app/forum', 'forum_button_my_topics'),
            'url'  => Url::toRoute(['my']),
            'icon'  => 'fas fa-crown',
            'class' => 'btn btn-default',
        ],
    ]); ?>`'
></forum-categories-list-item>

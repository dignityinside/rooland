import Vue from 'vue';

window.Vue = require('vue');

Vue.component('footer-block', require('./components/blocks/FooterBlock.vue').default);
Vue.component('widget', require('./components/widgets/Widget.vue').default);
Vue.component('widget-social', require('./components/widgets/Social.vue').default);
Vue.component('widget-ad', require('./components/widgets/Ad.vue').default);
Vue.component('widget-categories', require('./components/widgets/Categories.vue').default);
Vue.component('widget-forum-comments', require('./components/forum/widgets/Comments.vue').default);
Vue.component('widget-forum-topics', require('./components/forum/widgets/Topics.vue').default);
Vue.component('forum-categories-list-item', require('./components/forum/categories/List.vue').default);
Vue.component('forum-topics-item', require('./components/forum/topics/Item.vue').default);
Vue.component('forum-topics-items', require('./components/forum/topics/Items.vue').default);
Vue.component('forum-topics-list', require('./components/forum/topics/List.vue').default);
Vue.component('share', require('./components/blocks/Share.vue').default);
Vue.component('material-sort', require('./components/blocks/MaterialSort.vue').default);
Vue.component('buttons', require('./components/blocks/Buttons.vue').default);

const app = new Vue({
  'el': '#app',
});

$(document).ready(function(){
  const $markdownEditor = $('.markdown-editor');

  if ($markdownEditor.length) {
    initEditor($markdownEditor);
  }
});

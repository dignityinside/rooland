<?php

namespace app\models;

use app\models\forum\Forum;

/**
 * Comment model
 *
 * @author Alexander Schilling
 *
 * @package app\models
 *
 * @property-read \yii\db\ActiveQuery $topics
 */
class Comment extends \demi\comments\common\models\Comment
{
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTopics()
    {
        return $this->hasOne(Forum::class, ['id' => 'material_id']);
    }

    /**
     * Return related forums for comments
     *
     * @return \yii\db\ActiveQuery
     */
    public function getForums()
    {
        return $this->hasMany(Forum::class, ['id' => 'material_id']);
    }

    /**
     * Returns new comments with topics
     *
     * @param int $count
     *
     * @return array
     */
    public static function getNewComments(int $count = 10): array
    {
        return self::find()
            ->select([
                'id',
                'text',
                'user_name',
                'user_id',
                'material_id'
            ])
            ->andWhere([
                'material_type' => Material::MATERIAL_FORUM_ID
            ])
            ->with([
                'user' => function (\yii\db\ActiveQuery $query) {
                    $query->select(['username', 'id']);
                },
                'forums' => function (\yii\db\ActiveQuery $query) {
                    $query->select([
                        'id',
                        'title'
                    ]);
                }
            ])
            ->orderBy('created_at DESC')
            ->limit($count)
            ->asArray()
            ->all();
    }
}

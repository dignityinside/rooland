const mix = require('laravel-mix');

mix.js('resources/app.js', 'web/js')
   .copy('resources/editor.js', 'web/js')
   .sass('resources/scss/app.scss', 'web/css');
